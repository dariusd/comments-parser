<?php
    $file_name = 'comments_data.txt';
    $date_format = 'd-m-Y H:i';
    $arr = array(
        //syntax: array('Label', 'site URL', 'HTML element ID', 'regex')
        array('X-Kom', 'http://www.x-kom.pl/index.php?news=1512', 'comment', 'Komentarze \((\d+)\)'),
        array('Tabletowo', 'http://www.tabletowo.pl/2013/10/18/asus-transformer-book-t100-w-cenie-1499-zlotych-w-polsce/#div-comment-398621', 'comments', 'Comments \((\d+)\)')
    );
    
    $result = array(); 
    $ret = array();
    $i = 0;
    $changed = 0;
    
    libxml_use_internal_errors(true);   //don't show in-browser warnings
    
    //loading file content 
    $file_lines = file($file_name);
    //and printing it on screen
    foreach ($file_lines as $line) {
        $ret[] = unserialize($line);
        
    }
    var_dump($ret);
    
    foreach($arr as $elem) {
        $site = file_get_contents($elem[1]);

        $dom = new DOMDocument();

        //capturing website HTML content
        $dom->loadHTML($site);
        $dom->preserveWhiteSpace = false;

        $div_comments = $dom->getElementById($elem[2]);
    //    $txt_comments = $div_comments->childNodes->item(1)->nodeValue;

        foreach($div_comments->childNodes as $child) {
            //checking against regex from $arr[$i][3]
            if (preg_match("/" . $elem[3]. "/i", $child->nodeValue, $matches)) {
                $result[$i] = array($elem[0], $matches[1], date($date_format));
                
                if($result[$i][1] !== $ret[$i][1]) {    //if comments count !== number of comments from file
                    $changed = 1;
                }
                else {
                    $changed = 0;
                }
                
                if($changed) {  
                    unlink($file_name);     //delete file and create new one with updated info of comments
                    
                    for($j = 0; $j < count($result); $j++) {   
                        //writing (serialized) result to file
                        file_put_contents($file_name, serialize($result[$j])."\n", FILE_APPEND);
                    }
                }
            }
        }
        $i++;
    }
    
?>
